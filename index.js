console.log("Hello, world!");
console.log("Hello, Batch 203!");

//comments:

//This is a single line comment. ctrl + /


/*This is a

Multi Line comment
ctrl + shift + /

*/

// Statements in programming are instructions that we tell the  computer to perform

// syntax in programming, it is the set rules that describe how statements must be constructed

// Variables

/*
  -it is used to contain data

  -Syntax is declaring Variables
  -let/const variableName;

  let = Variables
  Const = fixed (e.g. Pi Value)


*/

let myVariable = "Hello";

console.log(myVariable);

/*console.log(hello);

let hello;
*/

let productName = 'desktop computer';
console.log (productName);

let product = "Alvin's computer";
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest)

productName ="Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

// interest = 4.489;
console.log(interest)



// Initialization
let supplier;

// Reassignment
supplier = "John Smith Tradings";
console.log(supplier)

// Reassignment
supplier = "Zuitt Store";
console.log(supplier)


const pi = 3.1416;
console.log(pi)


/*console.error(
a=5;
console.log(a);
let a;*/


// Multiple variable declarations
let productCode = "DC017";

const productBrand = "Dell";

console.log(productCode);

console.log(productBrand);

// const let ="hello";
// console.log(let);

// [SECTION] Data Types

// Strings
// Strings are series of characters that create a word, phrase, a sentence or anything related to creating text.

let country = 'Philippines';
let province = "Metro Manila";


// concatenation Strings
let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = "I love in the " + country;
console.log(greeting);


// escape Character (\)
// "\n" refers to creating new line or set the text to next Line
let mailAddress = "Metro Manila \n Philippines";
console.log(mailAddress);

let message = "John's employees went home early"
console.log(message);

message = 'John\'s employees went home early ezpz'
console.log(message);

// numbers
// intergers/wholenumbers

let headcount =26 ;
console.log(headcount);

// decimal Numbers/fractions
let grade = 98.7;
console.log(grade);


// Exponential notation
let planetDistance =2e10;
console.log(planetDistance);

// combine number and Strings
console.log("John's grade last quarter is " + grade);

// Boolean
// True/False

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: "+ isMarried);
console.log("isGoodConduct: " + isGoodConduct);


// Arrays
// it is used to store multiple values with similar data type.

// let/const arrayName = [elementA, elementB, element C, ...]

let grades= [98.7, 92.1, 90.2, 94.6];
console.log(grades);


let details = ["John", "Smith", 32, true];
console.log(details);

// objects
// objects are another special kind of data type that's used to mimic real world objects/items
// Syntax

/*
  let/const objectName ={
  propertyA: value,
  PropertyB:value
}


*/

let person = {
  fullname: "Juan Dela Cruz",
  age: 35,
  isMarried: false,
  contact:["+63917 123 4567", "81523 4567"],
  address:{
    houseNumber: "345",
    city: "Manila"
  }
}

console.log(person)


// Create abstract objects

let myGrades = {
  firstGrading: 98.7,
  secondGrading: 92.1,
  thirdGrading: 90.2,
  foruthGrading: 94.6
}

console.log(myGrades);

// typeof operator is used to determine the type of data or the variable value.

console.log (typeof myGrades);

console.log(typeof grades);


// Contstant objects and Arrays ;



const anime = ["one piece", "One punch man", "Attack on Titan"];
anime[0] = "kimetsu no yaiba";

console.log(anime);

// Null
// It is used to intentionally express the absence of the value in a variable declaration/Initialization.

let spouse = null;
console.log(spouse);


// undefined\
// represents the state of a variable that has been decared but without an asigned value.
let fullName;
console.log(fullName);
